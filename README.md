## USERS REST API

### This RESTful API has been implemented using Ballerina Lang

curl http://localhost:8080/users/all - Fetch all users curl
http://localhost:8080/users/create -d '{"username": "jhonny","firstname":"John",
"lastname: "Smith"}' - Create a new user curl
http://localhost:8080/users/single/[username] - Fetch a single user by username
curl http://localhost:8080/users/update/[username] -d
'{"username":"jhonny","firstname": "John", "lastname: "Jones"}' - Update an
existing user curl http://localhost:8080/users/delete/[username] - Remove an
existing user
