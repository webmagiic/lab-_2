import ballerina/io;
import ballerina/http;

type UserDetail record {|
    string username;
    string firstname;
    string lastname;
|};

UserDetail[] users = [];

service http:Service /users on new http:Listener(8080) {

    //@route GET /users
    //@desc Fetch All Users
    resource function get all() returns json {

        io:println("Handling GET request on /users");
        return users;
    }

    //@route POST /users/create
    //@desc Create a New User Record
    resource function post create(@http:Payload UserDetail request) returns json {
        users.push(request);
        io:println("Handling POST request on /users/create");
        return {"userName": request.username, "firstName": request.firstname, "lastName": request.lastname};
    }

    //@route GET /users/username
    //@desc Fetch a Single User 
    resource function get single/[string username]() returns json {

        io:println("Handling GET on /users/" + username);
        foreach int i in 0 ..< users.length() {
            //Get username values
            string member = users[i].get("username");
            //if value is equal to query parameter, return the user in json format
            if member == username {
                return users[i];
            }
        }

        return "No user found with that username";
    }

    //@Route PUT/PATCH /users/update/username
    //@desc Update User Record
    resource function post update/[string username](@http:Payload UserDetail request) returns json {

        io:println("Handling PUT/PATCH request on /users/update/" + username);

        foreach int i in 0 ..< users.length() {

            if users[i].username.indexOf(username) != () {
                users[i] = {...request};
                return users[i];
            }
        }
        return "No user found with that username";
    }

    //@route DELETE /users/delete/username
    //@desc Delete User Record
    resource function get delete/[string username]() returns json {
        io:println("Handling DELETE request on /users/delete/" + username);
        UserDetail[] filteredUsers = users.filter(user => user.get("username") != username);

        foreach var user in filteredUsers {

            if user.username.indexOf(username) == () {

                return "User has been deleted successfully";
            }
        }

        return "No user found with that username";
    }
}
